
import java.util.HashMap;
import java.util.Map;

public class Task1 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        String string = "My name is . is name My";
        String[] strings = string.split(" ");
        for (String str : strings) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
            } else {
                int value = map.get(str);
                value++;
                map.put(str, value);
            }
        }
        System.out.println(map);
    }
}