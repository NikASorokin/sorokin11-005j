
import java.util.HashMap;
import java.util.Map;

public class Task2 {
    public static void main(String[] args) {
        HashMap<String, Map<String, Integer>> map = new HashMap<>();
        String[] strings = new String[6];
        strings[0] = "Вася ручка 5";
        strings[1] = "Вася карандаш 3";
        strings[2] = "Петя книга 2";
        strings[3] = "Петя книга 3";
        strings[4] = "Вася книга 1";
        strings[5] = "Петя ручка 4";
        for (String str : strings) {
            String[] temp = str.split(" ");
            if(!map.containsKey(temp[0])){
                Map<String,Integer> mapTemp = new HashMap<>();
                mapTemp.put(temp[1], Integer.parseInt(temp[2]));
                map.put(temp[0],mapTemp);
            }else{if (!map.get(temp[0]).containsKey(temp[1])) {
                map.get(temp[0]).put(temp[1], Integer.parseInt(temp[2]));

            } else{
                int value = map.get(temp[0]).get(temp[1]);
                value += Integer.parseInt(temp[2]);
                map.get(temp[0]).put(temp[1], value);
            }
            }
        }
        System.out.println(map);
    }
}
